/******************************************************************************
 * @file:   bzenvarstr.h
 * @brief:  Variable length single byte sequence (e.g. ASCII) string.
 *
 * bzen_varstr_t is adapted from the VSTRING interface, written by 
 * Venema Wietse as part of the Postfix MTA under The Secure Mailer license.
 * 
 * @copyright:	Copyright (C) 2020 Kuhrman Technology Solutions LLC
 * @license:	GPLv3+: GNU GPL version 3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/

#ifndef _BZEN_VARSTR_H_
#define _BZEN_VARSTR_H_

#include <config.h>
#include "bzenvarchar.h"

/******************************************************************************
 * @TODO: THIS IS API
 *****************************************************************************/

/* extern VSTRING *vstring_strncat(VSTRING *, const char *, ssize_t); */

/******************************************************************************
 * THIS IS NOT API
 *****************************************************************************/

#define	BZEN_VARSTR_PUT(pstr, ch)	BZEN_VARCHAR_PUT(&(pstr)->varchar, ch)

/* Dynamically increase buffer size. */

/* Reset internal pointer at beginning of data buffer. */
 
#endif /* _BZEN_VARSTR_H_ */