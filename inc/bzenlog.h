/**
 * @file:	bzenlog.h
 * @brief:	Private logging facility functions.
 * 
 * @copyright:	Copyright (C) 2017 Kuhrman Technology Solutions LLC
 * @license:	GPLv3+: GNU GPL version 3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _BZEN_LOG_H_
#define _BZEN_LOG_H_

#include <config.h>

/**
 * Write formatted message to appropriate output stream.
 *
 * @param unsigned bzenlog_severity_code_t code Log severity level.
 * @param const char* fmt Message tormat string.
 * @param va_list Variable argument list.
 *
 * @return int Number of bytes written.
 */
int _bzen_write_vmsg(bzenlog_severity_code_t code, const char* fmt, ...);

/**
 * Writes a message to a file.
 *
 * @param unsigned bzenlog_severity_code_t code Log severity level.
 * @param const char* message Formatted message to log.
 *
 * @return int Number of bytes written.
 */
int _bzen_write_file(bzenlog_severity_code_t code, const char* message);

/**
 * Writes a message to the syslog.
 *
 * @param unsigned bzenlog_severity_code_t code Log severity level.
 * @param const char* message Formatted message to log.
 *
 * @return int Number of bytes written.
 */
int _bzen_write_syslog(bzenlog_severity_code_t code, const char* message);

/**
 * Returns the full path to the log files directory.
 *
 * Function will first seek an accessible directory defined by the environment
 * variable BZEN_LOG_DIR. If this fails, it will look for an accessible directory
 * relative to the PWD of the program. If both searches fail, it returns NULL.
 *
 * @return const char* path.
 */
const char* bzen_log_dir();

/**
 * Generate event line text.
 *
 * Event lines are 80 chars plus terminating null and are dynamically allocated
 * on the heap. It is the callers responsibility to free memory after use. If an
 * invalid severity code is passed, default is  BZENLOG_INFO.
 *
 * @param bzenlog_severity_code_t code Severity code. 
 * @param char* buffer Buffer to write to.
 * @param size_t size Size of buffer.
 *
 * @return int 0 on SUCCESS otherwise -1.
 */
static int  bzen_log_event_line(bzenlog_severity_code_t code,
				char* buffer,
				size_t size);

/**
 * Find corresponding id of named log if exists.
 *
 * @param const char* name Name of log to search for.
 *
 * @return int Id of log file >= 0 if exists otherwise -1.
 */
static int bzen_log_find_id(const char* name);

/**
 * Formats a log message so no line exceeds max chars and no words are broken.
 *
 * @param const char* message Unformatted message.
 * @param char* buffer Buffer to write to.
 * @param size_t size Size of buffer.
 *
 * @return int 0 on SUCCESS otherwise -1.
 */
static int  bzen_log_format_message(const char* message,
				    char* buffer,
				    size_t size);

/**
 * Report to syslog failure to access log resource.
 *
 * @param const char* package Name of package reporting error.
 * @param const char* resource Name of resource package could not access.
 * @param const char* function Name of function which returned a fail code.
 * @param const int code Error code returned by access function.
 *
 * @return void.
 */
static void bzen_log_handle_access_fail(const char* package,
					const char* resource,
					const char* function,
					int code);

/**
 * Check if given log file is open. 
 *
 * @param const char* name Name of log file.
 *
 * @return int 0 If log file is open, otherwise -1.
 */
int bzen_log_is_open(const char* name);

#endif /* _BZEN_LOG_H_ */