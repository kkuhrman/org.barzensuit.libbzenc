/******************************************************************************
 * @file:   bzenvarchar.h
 * @brief:  Variable length single byte (char) array.
 *
 * bzen_varchar_t is adapted from the VBUF interface, written by Venema Wietse
 * as part of the Postfix MTA under The Secure Mailer license.
 * 
 * @copyright:	Copyright (C) 2020 Kuhrman Technology Solutions LLC
 * @license:	GPLv3+: GNU GPL version 3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/

#ifndef _BZEN_VARCHAR_H_
#define _BZEN_VARCHAR_H_

#include <config.h>

/******************************************************************************
 * THIS CODE IS NOT API
 *****************************************************************************/
 
/**
 * Generic read operation.
 */
#define BZEN_VARCHAR_GET(pbuf)	((pbuf)->count < 0 ? ++(pbuf)->count, \
				(int) *(pbuf)->pos++ : bzen_varchar_get(pbuf))

/**
 * Generic write operation.
 */
#define BZEN_VARCHAR_PUT(pbuf, ch)	((pbuf)->count > 0 ? --(pbuf)->count, \
				(int) (*(pbuf)->pos++ = (ch)) : bzen_varchar_put(pbuf, ch))
				
/**
 * Generic memory allocation operation.
 */
#define BZEN_VARCHAR_ALLOC(pbuf, bytes) ((pbuf)->alloc_size((pbuf), (bytes)))

#endif /* _BZEN_VARCHAR_H_ */