/******************************************************************************
 * @file:	bzenapi.h
 * @brief:      Public Interface (API).
 *
 * @copyright:	Copyright (C) 2017-2020 Kuhrman Technology Solutions LLC
 * @license:	GPLv3+: GNU GPL version 3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/

#ifndef _BZEN_API_H_
#define _BZEN_API_H_

#include <config.h>

/******************************************************************************
 * @todo: debugging support should be defined in configure.ac
 *****************************************************************************/
#ifndef _BZEN_DEBUG_MODE_
#define _BZEN_DEBUG_MODE_ 1
#include <assert.h>
#endif /* _BZEN_DEBUG_MODE_ */

/******************************************************************************
 * Important types
 *****************************************************************************/

#include "errno.h"
#include "stddef.h"
#include "stdint.h"

/******************************************************************************
 * string types and API
 *****************************************************************************/
#define BZEN_SIZE_MAX SIZE_MAX

typedef struct _BZEN_VARCHAR_S bzen_varchar_t;
typedef int (*_BZEN_VARCHAR_GET_READY_FN) (bzen_varchar_t*);
typedef int (*_BZEN_VARCHAR_PUT_READY_FN) (bzen_varchar_t*);
typedef int (*_BZEN_VARCHAR_ALLOC_FN) (bzen_varchar_t*, size_t);

struct _BZEN_VARCHAR_S {
  /* Status, see below */  
  int flags;
  
  /* Size of the buffer in bytes. */  
  size_t length;
  
  /* Number of bytes left to read/write */
  size_t count;
  
  /* Random access position (internal) pointer. */
  unsigned char* pos;
  
  /* Pointer to buffer. */
  unsigned char* data;
  
  /* Read buffer empty action */
  _BZEN_VARCHAR_GET_READY_FN get_ready;
  
  /* Write buffer full action */
  _BZEN_VARCHAR_PUT_READY_FN put_ready;
  
  /* Request for buffer space */
  _BZEN_VARCHAR_ALLOC_FN alloc_size;
};

/*
  * Buffer status management.
  */
#define BZEN_VARCHAR_FLAG_RD_ERR	(1<<0)	/* read error */
#define BZEN_VARCHAR_FLAG_WR_ERR	(1<<1)	/* write error */
#define BZEN_VARCHAR_FLAG_ERR		(BZEN_VARCHAR_FLAG_RD_ERR | BZEN_VARCHAR_FLAG_WR_ERR)
#define BZEN_VARCHAR_FLAG_EOF		(1<<2)	/* end of data */
#define BZEN_VARCHAR_FLAG_RD_TIMEOUT	(1<<3)	/* read timeout */
#define BZEN_VARCHAR_FLAG_WR_TIMEOUT	(1<<4)	/* write timeout */
#define BZEN_VARCHAR_FLAG_TIMEOUT	(BZEN_VARCHAR_FLAG_RD_TIMEOUT | BZEN_VARCHAR_FLAG_WR_TIMEOUT)
#define BZEN_VARCHAR_FLAG_BAD	(BZEN_VARCHAR_FLAG_ERR | BZEN_VARCHAR_FLAG_EOF | BZEN_VARCHAR_FLAG_TIMEOUT)
#define BZEN_VARCHAR_FLAG_FIXED		(1<<5)	/* fixed-size buffer */

#define bzen_varchar_rd_error(v) ((v)->flags & (VBUF_FLAG_RD_ERR | VBUF_FLAG_RD_TIMEOUT))
#define bzen_varchar_wr_error(v) ((v)->flags & (VBUF_FLAG_WR_ERR | VBUF_FLAG_WR_TIMEOUT))
#define bzen_varchar_rd_timeout(v)	((v)->flags & VBUF_FLAG_RD_TIMEOUT)
#define bzen_varchar_wr_timeout(v)	((v)->flags & VBUF_FLAG_WR_TIMEOUT)

#define bzen_varchar_error(v)	((v)->flags & (VBUF_FLAG_ERR | VBUF_FLAG_TIMEOUT))
#define bzen_varchar_eof(v)	((v)->flags & VBUF_FLAG_EOF)
#define bzen_varchar_timeout(v)	((v)->flags & VBUF_FLAG_TIMEOUT)
#define bzen_varchar_clearerr(v) ((v)->flags &= ~VBUF_FLAG_BAD)

/**
 * End of file/buffer.
 */
#define BZEN_VARCHAR_EOF	(-1)

/**
 * Read next character from the given buffer.
 * 
 * @param bzen_varchar* pbuf Buffer to read from.
 *
 * @return int
 */
extern int bzen_varchar_get(bzen_varchar_t* pbuf);

/**
 * Write character to the given buffer.
 *
 * @param bzen_varchar* pbuf Buffer to write to.
 * @param int ch Character to write.
 *
 * @return int
 */
extern int bzen_varchar_put(bzen_varchar_t* pbuf, int ch);

/**
 * Bulk read from buffer 
 *
 * @param bzen_varchar* preadbuf Buffer to read from.
 * @param void* pwritebuf Buffer to write to.
 * @param size_t len Number of bytes to read.
 *
 * @return size_t Number of bytes read.
 */
extern size_t bzen_varchar_read(bzen_varchar_t* preadbuf, void* pwritebuf, size_t len);

/**
 * Implement at least one character push back
 * @param bzen_varchar* pbuf Buffer to write to.
 * @param int ch Character to push back.
 *
 * @return int 
 */
extern int bzen_varchar_unget(bzen_varchar_t* pbuf, int ch);

/**
 * Bulk write to buffer 
 *
 * @param bzen_varchar*  pwritebuf Buffer to write to.
 * @param const void* preadbuf Buffer to read from.
 * @param size_t len Number of bytes to write.
 *
 * @return size_t Number of bytes written.
 */
size_t bzen_varchar_write(bzen_varchar_t* pwritebuf, const void* preadbuf, size_t len);

typedef struct _BZEN_VARSTR_S bzen_varstr_t;

struct _BZEN_VARSTR_S {
  bzen_varchar_t varchar;
};

/**
 * Flags 24..31 are reserved for bzen_varstr_t. 
 */
/**
 * Exact allocation for tests 
 */
#define BZEN_VARSTR_FLAG_EXACT	(1<<24)

/**
 * Allocate memory for a variable string of the given size.
 *
 * @param size_t len Size of the variable string in bytes.
 *
 * @return bzen_varstr* Pointer to the newly allocated string.
 */
extern bzen_varstr_t* bzen_varstr_create(size_t len);

/**
 * Append one string to another. 
 *
 * @param bzen_varstr_t* pstr String to append to.
 * @param const char* src String to append.
 *
 * @return bzen_varstr_t*
 */
extern bzen_varstr_t* bzen_varstr_strcat(bzen_varstr_t* pstr, const char *src);

/** 
 * Append limited number of bytes from one string to another. 
 *
 * @param bzen_varstr_t* pstr String to append to.
 * @param const char* src String to copy from.
 * @param size_t len Number of bytes to copy.
 *
 * @return bzen_varstr_t* 
 */
bzen_varstr_t* bzen_varstr_strncat(bzen_varstr_t* pstr, const char *src, size_t len);

/** 
 * Copy content of one string into another. 
 *
 * @param bzen_varstr_t* pstr String to copy to.
 * @param const char* src String to copy from.
 *
 * @return bzen_varstr_t*  
 */
extern bzen_varstr_t* bzen_varstr_strcpy(bzen_varstr_t* pstr, const char* src);

/**
 * Copy limited number of bytes from one string to another. 
 *
 * @param bzen_varstr_t* pstr String to copy to.
 * @param const char* src String to copy from.
 * @param size_t len Number of bytes to copy.
 *
 * @return bzen_varstr_t*   
 */
bzen_varstr_t* bzen_varstr_strncpy(bzen_varstr_t* pstr, const char *src, size_t len);

/**
 * Free memory allocated to the given variable string.
 *
 * @param bzen_varstr* vstr Variable string containing memory block to free.
 *
 * @return bzen_varstr* Variable string after memory free.
 */
extern bzen_varstr_t* bzen_varstr_free(bzen_varstr_t* vstr);

/********************************************************************************
 * Logging API
 * 
 * Log conventions for Project Barzensuit:
 *
 * 1. All logs are stored in a shared directory (currently manually configured).
 * 2. Error logs are given the name [package]-error
 * 3. Logs containing non-error messages are named after their parent package.
 * 4. Log lines are limited to 80 characters (not counting new line).
 * 5  Each logged message consists of two or more lines, event data and message.
 * 6. The event data consists of the following:
 * 6.1  Event fields are space delimited
 * 6.2  10 character date field YYYY-mm-dd
 * 6.3  8 character time field HH:MM:SS
 * 6.4  4 characters reserved for micro time
 * 6.4  1 character severity code field
 * 6.5  4 x 5 character fields for real and effective user/group ids
 * 6.6  The remaining 31 characters are reserved and currently not in use.
 * 7. The message occupies one or more lines after the event line.
 * 7.1  Message text is word wrapped with no line occupying more than 80 chars.
 *
 *******************************************************************************/

/**
 * Maximum length of message written to logging facility.
 */
#define BZEN_LOG_MESSAGE_MAX_CHARS 1024

/**
 * Default fopen() attribute (append).
 */
#define BZEN_LOG_FOPEN_DEFAULT_ATTR "a"

/**
 * @enum Log event severity codes.
 */
enum BZENLOG_SEVERITY_CODE 
  {
    BZENLOG_ERROR = 0, /* E */
    BZENLOG_WARNING,   /* W */
    BZENLOG_STATUS,    /* S */
    BZENLOG_INFO,      /* I */
    BZENLOG_DEBUG      /* D */
  };
  

/**
 * @typedef bzenlog_severity_code_t
 */
typedef unsigned short int bzenlog_severity_code_t;

/* Send a message to the error log. */
int bzen_log_error(const char*, ...);

/* Send a message to the warning log. */
int bzen_log_warn(const char*, ...);

/* Send a message to the status log. */
int bzen_log_status(const char*, ...);

/* Send a message to the info log. */
int bzen_log_info(const char*, ...);

/* Send a message to the debug log. */
int bzen_log_debug(const char*, ...);
 
 /**
 * Close the given named log.
 *
 * The 'close()' functions simply close the encapsulated log file.
 * They do not free memory allocated on the heap nor do they effect
 * mutexes. The log files can be reopened and used again.
 *
 * Use the 'destroy()' functions to free memory on the heap and 
 * destroy mutexes.
 *
 * @param const char* name Name of log file to close.
 *
 * @return int 0 If log file is closed, otherwise -1.
 */
int bzen_log_close(const char* name);
  
  /**
 * Open a log file with given attributes. 
 *
 * @param const char* name Name of log file.
 * @param const char* attr Open attributes (e.g. 'r', 'w') @see fopen().
 *
 * @return int 0 If log file is open, otherwise -1.
 */
int bzen_log_open(const char* name, const char* attr);

/**
 * Write a message to the given log.
 *
 * @param const char* name The name of the log to write to.
 * @param bzenlog_severity_code_t code Severity code. 
 * @param const char* message The message to write.
 *
 * @return int 0 if the message was written otherwise -1.
 */
int bzen_log_write(const char* name, 
                   bzenlog_severity_code_t code, 
                   const char* message);

/**
 * Write a message to the given log. Open log file if necessary.
 *
 * The purpose of bzen_log_write_stat() is to send messages to the log reliably
 * while maintaining the state of the target log file and minimizing the amount
 * of code to get the job done. The typical requirements to log a message are to
 * open the file, write the message and close the file. Of course, repeated open
 * and close calls may be expensive and must be weighed against the cost of 
 * keeping the file open indefinitely. This function allows the developer to 
 * decide which strategy suits their needs best and also allows others to send
 * messages to the log reliably without changing the open/close strategy in 
 * effect. If the file is closed, bzen_log_write_stat() will open it, write the
 * message and close it again. If the file is open bzen_log_write_stat() will 
 * write the message and leave the file open.
 *
 * @param const char* name The name of the log to write to.
 * @param bzenlog_severity_code_t code Severity code. 
 * @param const char* message The message to write.
 *
 * @return int 0 if the message was written otherwise -1.
 */
int bzen_log_write_stat(const char* name, 
                        bzenlog_severity_code_t code, 
                        const char* message);

/********************************************************************************
 * Sockets API
 *******************************************************************************/

#include <netinet/in.h>
#include <sys/socket.h>

/**
 * Create an internet socket address.
 * 
 * @param uint32_t host Binary representation of IPv4 address.
 * @param uint32_t port
 *
 * @return struct sockaddr_in*
 */
struct sockaddr_in* bzen_socket_address_in(uint32_t host, uint32_t port);

/**
 * Create a local socket address.
 * 
 * @param const char* name Name socket address is registered under.
 * @param socklen_t* address_size Optional, returns size of address.
 *
 * @return struct sockaddr_un* The newly created address.
 */
struct sockaddr_un* bzen_socket_address_un(const char* name);

/**
 * Bind a socket to the given address.
 *
 * @param int socket_fd 
 * @param struct sockaddr* address
 * @param socklen_t address_size
 * 
 * @return int 0 on success and -1 on failure. 
 */
int bzen_socket_bind(int socket_fd, 
		     struct sockaddr* address, 
		     socklen_t address_size);

/**
 * Close given socket.
 *
 * @param int socket_fd File descriptor of socket to shutdown.
 * @param int how SHUT_RD | SHUT_WR | SHUT_RDWR
 *
 * @return int 0 on success and -1 on failure.
 */
int bzen_socket_close(int socket_fd, int how);

/**
 * Request connection with  socket at  given address.
 *
 * @param int socket_fd 
 * @param struct sockaddr* address
 * @param socklen_t address_size
 * 
 * @return int 0 on success and -1 on failure. 
 */
int bzen_socket_connect(int socket_fd, 
			struct sockaddr* address, 
			socklen_t address_size);

/**
 * Enable connection requests on server socket.
 *
 * @param int socket_fd File decriptor of server socket.
 * @param int queue_len Number of connection requests to accept.
 *
 * @return int 0 on success and -1 on failure.
 */
int bzen_socket_listen(int socket_fd, int queue_len);

/**
 * Open a socket.
 *
 * @param int namespace
 * @param int style
 * @param int protocol
 *
 * @return int File descriptor of the newly opened socket.
 */
int bzen_socket_open(int namespace, int style, int protocol);

/**
 * Receive data from connected peer.
 *
 * @param in socket_fd File descriptor of receiving socket.
 * @param void* buffer Pointer to buffer serving as data sink.
 * @param size_t data_size Number of bytes to read.
 * @param int flags MSG_OOB | MSG_PEEK | MSG_DONTROUTE | 0.
 * 
 * @return size_t Number of bytes received.
 */
ssize_t bzen_socket_receive(int socket_fd, void* data, size_t data_size, int flags);

/**
 * Send data to connected peer.
 *
 * @param int socket_fd File descriptor of transmitting socket.
 * @param const void* data Pointer to buffer containing transmission data.
 * @param size_t data_size Size in bytes of data to transmit.
 * @param int flags MSG_OOB | MSG_DONTROUTE | 0
 *
 * @return size_t Number of bytes transmitted.
 */
ssize_t bzen_socket_send(int socket_fd, const void* data, size_t data_size, int flags);

/********************************************************************************
 * Stream and buffering API
 *******************************************************************************/

/**
 * @typedef bzen_cbuflock
 */
typedef struct _bzen_cbuflock_s {
  unsigned short int id;
  unsigned short int keep_open;
  pthread_mutex_t mutex;
  size_t size;
} bzen_cbuflock_t;

/**
 * Allocate memory for a new character buffer.
 *
 * @param size_t size Size in bytes of buffer.
 *
 * @return bzen_cbuflock_t* Pointer to new buffer lock.
 */
bzen_cbuflock_t* bzen_sbuf_create(size_t size);

/**
 * Put a character to the given buffer.
 *
 * The fputc function converts the character c to type unsigned char, and writes
 * it to the stream stream. EOF is returned if a write error occurs; otherwise
 *  the character c is returned.
 *
 * @param int c The character to put.
 * @param bzen_cbuflock_t* cbuflock Pointer to lock for buffer.
 *
 * @return int The put character or EOF.
 */
int bzen_sbuf_putc(int c, bzen_cbuflock_t* cbuflock);

/********************************************************************************
 * Threading API
 *******************************************************************************/

#include <pthread.h>
 
/**
 * Encapsulates pthread_create().
 *
 * If NULL is passed for attr, defaults are used.
 *
 * @param pthread_t* thread Will stire ID of created thread.
 * @param  const pthread_attr_t* attr Specifies attributes of thread
 * @param void* (*start_routine)(void*) routine Thread routine.
 * @param void* arg Sole argument passed to thread routine.
 *
 * @return int 0 on SUCCESS otherwise errno.
 */
int bzen_thread_create(pthread_t *thread,
		       const pthread_attr_t *attr,
		       void* (*start_routine)(void*),
		       void *arg);

/**
 * Encapsulates pthread_join().
 *
 * @param pthread_t thread Target thread.
 * @param void** value_ptr Will store calue of pthread_exit().
 *
 * @return 0 on SUCCESS otherwise errno.
 */
int bzen_thread_join(pthread_t thread, void** value_ptr);

#endif /* _BZEN_API_H_ */