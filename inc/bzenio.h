/**
 * @file:	bzenio.h
 * @brief:	Encapsulate GnuLib IO wrapper functions.
 *
 * @copyright:	Copyright (C) 2019 Kuhrman Technology Solutions LLC
 * @license:	GPLv3+: GNU GPL version 3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
 #ifndef __BZEN_IO_H
 #define __BZEN_IO_H
 
#include <config.h>
#include <fwritable.h>

/**
 * Verify if stream supports writing.
 * 
 * @param FILE* stream The IO stream to check.
 *
 * @return boolean TRUE if stream supports writing otherwise FALSE.
 */
 bool bzen_fwritable(FILE* stream);
 
 #endif /** __BZEN_IO_H **/