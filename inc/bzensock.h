/**
 * @file:	bzensock.h
 * @brief:	Portability API for POSIX/Windows sockets.
 * 
 * @todo: V. 0.1.2
 * Wrappers only. Support for POSIX connectionless sockets only.
 * socket(), bind(), send(), sendto(), recv(), recvfrom(), shutdown()
 * sockaddr_un
 *
 * @todo: V. 0.1.3
 * Wrappers only. Support for POSIX Internet sockets added.
 * listen(), accept(), connect()
 * sockaddr_in
 *
 * @todo: V. 0.1.4
 * threadsafe support and clean up
 *
 * @todo: V. 0.1.5
 * add sockaddr_in6 and host names support
 *
 * @copyright:	Copyright (C) 2017 Kuhrman Technology Solutions LLC
 * @license:	GPLv3+: GNU GPL version 3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _BZENLIBC_SOCK_H_
#define _BZENLIBC_SOCK_H_

#include <errno.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

/* libzenc includes */

/* A default prefix for things such as local addresses. */
#define BZENSOCK_PREFIX "BZENSOCK_"

/* socket is on server or client end of connection. */
#define BZENSOCK_SERVER 0x00000100
#define BZENSOCK_CLIENT 0x00001000

/**
 * Accept connection request.
 *
 * @param int socket_fd 
 * @param struct sockaddr* address Optional, out
 * @param socklen_t* address_size Optional, out
 * 
 * @return int File descriptor of server side connection or -1.
 */
int bzen_socket_accept(int socket_fd, 
		       struct sockaddr* address, 
		       socklen_t* address_size);

/**
 * @todo: struct sockaddr_in6* bzen_socket_adrdess_in6();
 */

/**
 * Receive data from connectionless peer.
 *
 * The address and address size parameters are optional and only used if it is 
 * necessary to know the address of the sender. Otherwise bzen_socket_receive()
 * should be used.
 *
 * @param in socket_fd File descriptor of receiving socket.
 * @param void* buffer Pointer to buffer serving as data sink.
 * @param size_t data_size Number of bytes to read.
 * @param int flags MSG_OOB | MSG_PEEK | MSG_DONTROUTE | 0.
 * @param struct sockaddr* address Address of sending socket.
 * @param socklen_t* address_size Size in bytes of sending address.
 * 
 * @return size_t Number of bytes received.
 */
ssize_t bzen_socket_receive_from(int socket_fd, 
				 void* data, 
				 size_t data_size, 
				 int flags,
				 struct sockaddr* address,
				 socklen_t* address_size);

/**
 * Send data to connectionless peer.
 *
 * @param int socket_fd File descriptor of transmitting socket.
 * @param const void* data Pointer to buffer containing transmission data.
 * @param size_t data_size Size in bytes of data to transmit.
 * @param int flags MSG_OOB | MSG_DONTROUTE | 0
 * @param struct sockaddr* address Address of destination socket.
 * @param socklen_t address_size Size in bytes of destination address.
 *
 * @return size_t Number of bytes transmitted.
 */
ssize_t bzen_socket_send_to(int socket_fd, 
			   const void* data, 
			   size_t data_size, 
			   int flags,
			   struct sockaddr* address, 
			   socklen_t address_size);

#endif /* _BZENLIBC_SOCK_H_ */