/******************************************************************************
 * @file:   bzenvarchar.c
 * @brief:  Variable length single byte (char) array.
 *
 * bzen_varchar_t is adapted from the VBUF interface, written by Venema Wietse
 * as part of the Postfix MTA under The Secure Mailer license.
 * 
 * @copyright:	Copyright (C) 2020 Kuhrman Technology Solutions LLC
 * @license:	GPLv3+: GNU GPL version 3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/

#include <config.h>

/* system libraries */
#include <string.h>

/* libbzenc */
#include "bzenapi.h"
#include "bzenvarchar.h"

/* Read next character from the given buffer. */
int bzen_varchar_get(bzen_varchar_t* pbuf) {
  return (pbuf->get_ready(pbuf) ?	((pbuf->flags |= BZEN_VARCHAR_FLAG_EOF), \
    BZEN_VARCHAR_EOF) : BZEN_VARCHAR_GET(pbuf));
}

/* Write character to the given buffer. */
int bzen_varchar_put(bzen_varchar_t* pbuf, int ch) {
  return (pbuf->put_ready(pbuf) ? BZEN_VARCHAR_EOF : \
    BZEN_VARCHAR_PUT(pbuf, ch));
}

/* Bulk read from buffer */
size_t bzen_varchar_read(bzen_varchar_t* preadbuf, void* pwritebuf, size_t len) {
  size_t count;
  void   *cp;
  size_t n;
  
  #if 0
    for (count = 0; count < len; count++) 
    {
      if ((pwritebuf[count] = BZEN_VARCHAR_GET(preadbuf)) < 0) 
      {
        break;
      }
    }
    return (count);
  #else
    for (cp = pwritebuf, count = len; count > 0; cp += n, count -= n) 
    {
      if (preadbuf->count >= 0 && preadbuf->get_ready(preadbuf))
      {
        break;
      }
      n = (count < -preadbuf->count ? count : -preadbuf->count);
      memcpy(cp, preadbuf->pos, n);
      preadbuf->pos += n;
      preadbuf->count += n;
    }
    return (len - count);
  #endif
}

/* Implement at least one character pushback */
int bzen_varchar_unget(bzen_varchar_t* pbuf, int ch) {
  int result = BZEN_VARCHAR_EOF;
  
  if ((ch & 0xff) != ch || -pbuf->count >= pbuf->length) 
  {
    /* This error affects reads! */
    pbuf->flags |= BZEN_VARCHAR_FLAG_RD_ERR;
  } 
  else 
  {
    pbuf->count--;
    pbuf->flags &= ~BZEN_VARCHAR_FLAG_EOF;
    result = (*--pbuf->pos = ch);
  }
  return result;
}

/* Bulk write to buffer */
size_t bzen_varchar_write(bzen_varchar_t* pwritebuf, const void* preadbuf, size_t len) {
  size_t count;
  const void *cp;
  size_t n;
  
  #if 0
    for (count = 0; count < len; count++) 
    {
      if (BZEN_VARCHAR_PUT(pwritebuf, preadbuf[count]) < 0)
      {
        break;
      }
    }
    return (count);
  #else
    for (cp = preadbuf, count = len; count > 0; cp += n, count -= n) 
    {
      if (pwritebuf->count <= 0 && pwritebuf->put_ready(pwritebuf) != 0)
      {
        break;
      }
      n = (count < pwritebuf->count ? count : pwritebuf->count);
      memcpy(pwritebuf->pos, cp, n);
      pwritebuf->pos += n;
      pwritebuf->count -= n;
    }
    return (len - count);
  #endif
}
