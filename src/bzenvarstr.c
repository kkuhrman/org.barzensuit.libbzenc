/******************************************************************************
 * @file:   bzenvarstr.c
 * @brief:  Variable length single byte sequence (e.g. ASCII) string.
 *
 * bzen_varstr_t is adapted from the VSTRING interface, written by 
 * Venema Wietse as part of the Postfix MTA under The Secure Mailer license.
 * 
 * @copyright:	Copyright (C) 2020 Kuhrman Technology Solutions LLC
 * @license:	GPLv3+: GNU GPL version 3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/

#include <config.h>

/* system library*/
#include <string.h>

/* libbzenc */
#include "bzenapi.h"
#include "bzenmem.h"
#include "bzenvarstr.h"

/*****************************************************************************
 * bzen_varstr_t private functions (NOT API))
 *****************************************************************************/

/* Dynamically increase buffer size. */
static void bzen_varstr_extend(bzen_varchar_t* pbuf, size_t len) {
  size_t number_used_bytes = pbuf->pos - pbuf->data;
  size_t request_new_size;
  size_t actual_new_size;

  /*
   * libzenc reacclocation strategy default attempts to avoid frequent, small
   * reallocations of memory by incrementing at a rate of 1.5 times current 
   * memory block size. This strategy is overridden if the flag 
   * BZEN_VARSTR_FLAG_EXACT is set or if the requested size of reallocation
   * is more than the current buffer size.
   * 
   * @see bzen_realloc()
   */
  if ((pbuf->flags & BZEN_VARSTR_FLAG_EXACT) == 0 && pbuf->length > len)
  {
    len = (pbuf->length)/2;
  }
  if (pbuf->length > BZEN_SIZE_MAX - len - 1) {
    /* @todo: implement msg_fatal("vstring_extend: length overflow"); */
  }
  request_new_size = pbuf->length + len;
  actual_new_size = request_new_size + 1;
  
  pbuf->data = (unsigned char*) bzen_realloc((void*) pbuf->data, 
    &actual_new_size, BZEN_SIZE(sizeof(unsigned char)));
  pbuf->data[actual_new_size] = 0;
  pbuf->length = actual_new_size;
  pbuf->pos = pbuf->data + number_used_bytes;
  pbuf->count = pbuf->length - number_used_bytes;
}

/* Reset internal pointer at beginning of data buffer. */
static void bzen_varstr_reset(bzen_varstr_t* pstr) {
  (pstr)->varchar.pos = (pstr)->varchar.data;
  (pstr)->varchar.count = (pstr)->varchar.length;
}

/* Null-terminate character array. */
static void bzen_varstr_terminate(bzen_varstr_t* pstr) {
  *(pstr)->varchar.pos = 0;
}

/*****************************************************************************
 * Implement the bzen_varchar_t interface
 *****************************************************************************/

/**
 * callback for read buffer empty condition 
 */
static int bzen_varstr_get_ready(bzen_varchar_t* pbuf) {
  return (BZEN_VARCHAR_EOF);
}

/**
 * callback for write buffer full condition 
 */
static int bzen_varstr_put_ready(bzen_varchar_t* pbuf) {
  bzen_varstr_extend(pbuf, 1);
  return (0);
}

/**
 * callback to reserve space 
 */
static int bzen_varstr_alloc(bzen_varchar_t* pbuf, size_t len) {
  size_t need;
  
  if (len < 0)
  {
    /* @todo: msg_panic("vstring_buf_space: bad length %ld", (long) len); */
    if ((need = len - pbuf->count) > 0) 
    {
      bzen_varstr_extend(pbuf, need);
    }
  }
  return (0);
}
 
/*****************************************************************************
 * bzen_varstr_t public functions (API))
 *****************************************************************************/
  
/* Allocate memory for a variable string of the given size. */
bzen_varstr_t* bzen_varstr_create(size_t len) {
  bzen_varstr_t* pvstr;
  
  if (len > BZEN_SIZE_MAX - 1) {
    /* @todo: log a warning and fail if invalid size is passed. */
  }

  /* allocate memory for the varstr struct */  
  pvstr = (bzen_varstr_t*)bzen_malloc(BZEN_SIZE(sizeof(bzen_varstr_t)));
  
  #ifdef _BZEN_DEBUG_MODE_
    assert(pvstr);
  #endif /*_BZEN_DEBUG_MODE_ */
  
  /* allocate memory for the encapsulated array. */
  pvstr->varchar.data = 
    (unsigned char*)bzen_malloc(BZEN_SIZE(sizeof(unsigned char)) * (len + 1));
    
  #ifdef _BZEN_DEBUG_MODE_
    assert(pvstr->varchar.data);
  #endif /*_BZEN_DEBUG_MODE_ */
  
  pvstr->varchar.length = len;
  
  /* NOTE: bzen_varstr adds its own null terminator at end of buffer. */
  pvstr->varchar.data[len] = 0;
  pvstr->varchar.data[0] = 0;
  pvstr->varchar.pos = pvstr->varchar.data;
  
  /* @todo: reset operation VSTRING_RESET(pstr); */

  /* interface implementation */  
  pvstr->varchar.get_ready = bzen_varstr_get_ready;  
  pvstr->varchar.put_ready = bzen_varstr_put_ready;
  pvstr->varchar.alloc_size = bzen_varstr_alloc;
  
  /* bzen_varstr member initialization */
  pvstr->varchar.flags = 0;
  
  return pvstr;
}

/* Free memory allocated to the given variable string. */
bzen_varstr_t* bzen_varstr_free(bzen_varstr_t* vstr) {
  
  /* @todo: log warning if null pointers are encountered */
  if (vstr && vstr->varchar.data) {
    bzen_free((void*)vstr->varchar.data);
  }
  bzen_free((void*)vstr);
  return NULL;
}

/* Append one string to another. */
bzen_varstr_t* bzen_varstr_strcat(bzen_varstr_t* pstr, const char *src) {
  while (*src) {
    BZEN_VARSTR_PUT(pstr, *src);
    src++;
  }
  bzen_varstr_terminate(pstr);
  return (pstr);
}

/* Append limited number of bytes from one string to another. */
bzen_varstr_t* bzen_varstr_strncat(bzen_varstr_t* pstr, const char *src, size_t len) {
  while (len-- > 0 && *src) {
    BZEN_VARSTR_PUT(pstr, *src);
    src++;
  }
  bzen_varstr_terminate(pstr);
  return (pstr);
}

/* Copy content of one string into another. */
bzen_varstr_t* bzen_varstr_strcpy(bzen_varstr_t* pstr, const char* src) {
  bzen_varstr_reset(pstr);
  
  while (*src) {
    BZEN_VARSTR_PUT(pstr, *src);
    src++;
  }
  bzen_varstr_terminate(pstr);
  return (pstr);
}

/* Copy length limited content of one string into another. */
bzen_varstr_t* bzen_varstr_strncpy(bzen_varstr_t* pstr, const char *src, size_t len) {
  bzen_varstr_reset(pstr);
  
  while (len-- > 0 && *src) {
    BZEN_VARSTR_PUT(pstr, *src);
    src++;
  }
  bzen_varstr_terminate(pstr);
  return (pstr);
}