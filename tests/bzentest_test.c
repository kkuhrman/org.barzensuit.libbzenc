/**
 * @file:	bzentest_test.c
 * @brief:	Unit test unit test helper functions.
 *
 * @copyright:	Copyright (C) 2017 Kuhrman Technology Solutions LLC
 * @license:	GPLv3+: GNU GPL version 3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>

#include "bzentest.h"

int
main (int argc, char *argv[])
{
  int result = BZEN_TEST_EVAL_PASS;

  /**
   * bzen_test_bool() is a pseudo (bool)int cast, which doesn't exist in ANSI C.
   * Return non-zero if value is non-zero, otherwise return zero.
   */
  /* Test bzen_test_bool() === BZENPASS */
  if (0 == bzen_test_bool (BZEN_TEST_BOOL_TRUE))
    {
      fprintf (
	  stderr,
	  "\n\tbzen_test_bool(BZEN_TEST_BOOL_TRUE) returned 0 expected non-zero.\n");
      result = BZEN_TEST_EVAL_FAIL;
      goto END_TEST;
    }

  /* Test bzen_test_bool() === BZENFAIL */
  if (0 != bzen_test_bool (BZEN_TEST_BOOL_FALSE))
    {
      fprintf (
	  stderr,
	  "\n\tbzen_test_bool(BZEN_TEST_BOOL_FALSE) returned non-zero expected 0.\n");
      result = BZEN_TEST_EVAL_FAIL;
      goto END_TEST;
    }

  /* actual equals expected should PASS */
  if (BZEN_TEST_EVAL_FAIL == bzen_test_equals_int (1, 1))
    {
      fprintf (
	  stderr,
	  "\n\tbzen_test_equals_int(1,1) returned BZEN_TEST_EVAL_FAIL expected BZEN_TEST_EVAL_PASS.\n");
      result = BZEN_TEST_EVAL_FAIL;
      goto END_TEST;
    }

  /* actual NOT equals expected should FAIL */
  if (BZEN_TEST_EVAL_PASS == bzen_test_equals_int (1, 0))
    {
      fprintf (
	  stderr,
	  "\n\tbzen_test_equals_int(1,0) returned BZEN_TEST_EVAL_PASS expected BZEN_TEST_EVAL_FAIL.\n");
      result = BZEN_TEST_EVAL_FAIL;
      goto END_TEST;
    }

  /* actual NOT equals expected should PASS */
  if (BZEN_TEST_EVAL_FAIL == bzen_test_not_equals_int (1, 0))
    {
      fprintf (
	  stderr,
	  "\n\tbzen_test_not_equals_int(1,0) returned BZEN_TEST_EVAL_FAIL expected BZEN_TEST_EVAL_PASS.\n");
      result = BZEN_TEST_EVAL_FAIL;
      goto END_TEST;
    }

  /* actual equals expected should FAIL */
  if (BZEN_TEST_EVAL_PASS == bzen_test_not_equals_int (1, 1))
    {
      fprintf (
	  stderr,
	  "\n\tbzen_test_not_equals_int(1,1) returned BZEN_TEST_EVAL_PASS expected BZEN_TEST_EVAL_FAIL.\n");
      result = BZEN_TEST_EVAL_FAIL;
      goto END_TEST;
    }

  /* FALSE should PASS */
  if (BZEN_TEST_EVAL_FAIL == bzen_test_logical_false (BZEN_TEST_BOOL_FALSE))
    {
      fprintf (
	  stderr,
	  "\n\tbzen_test_logical_false(BZEN_TEST_BOOL_FALSE) returned BZEN_TEST_EVAL_FAIL expected BZEN_TEST_EVAL_PASS.\n");
      result = BZEN_TEST_EVAL_FAIL;
      goto END_TEST;
    }

  /* TRUE should FAIL */
  if (BZEN_TEST_EVAL_PASS == bzen_test_logical_false (BZEN_TEST_BOOL_TRUE))
    {
      fprintf (
	  stderr,
	  "\n\tbzen_test_logical_false(BZEN_TEST_BOOL_TRUE) returned BZEN_TEST_EVAL_PASS expected BZEN_TEST_EVAL_FAIL.\n");
      result = BZEN_TEST_EVAL_FAIL;
      goto END_TEST;
    }

  /* TRUE should PASS */
  if (BZEN_TEST_EVAL_FAIL == bzen_test_logical_true (BZEN_TEST_BOOL_TRUE))
    {
      fprintf (
	  stderr,
	  "\n\bzen_test_logical_true(BZEN_TEST_BOOL_TRUE) returned BZEN_TEST_EVAL_FAIL expected BZEN_TEST_EVAL_PASS.\n");
      result = BZEN_TEST_EVAL_FAIL;
      goto END_TEST;
    }

  /* FALSE should FAIL */
  if (BZEN_TEST_EVAL_PASS == bzen_test_logical_true (BZEN_TEST_BOOL_FALSE))
    {
      fprintf (
	  stderr,
	  "\n\bzen_test_logical_true(BZEN_TEST_BOOL_FALSE) returned BZEN_TEST_EVAL_PASS expected BZEN_TEST_EVAL_FAIL.\n");
      result = BZEN_TEST_EVAL_FAIL;
      goto END_TEST;
    }

  /* actual equals expected should PASS */
  if (BZEN_TEST_EVAL_FAIL
      == bzen_test_eval_fn_bool ("testFunction", BZEN_TEST_BOOL_TRUE,
				 BZEN_TEST_BOOL_TRUE, NULL))
    {
      fprintf (
	  stderr,
	  "\n\bzen_test_eval_fn_bool (NULL, BZEN_TEST_BOOL_TRUE, BZEN_TEST_BOOL_TRUE, NULL) \
	   returned BZEN_TEST_EVAL_FAIL expected BZEN_TEST_EVAL_PASS.\n");
      result = BZEN_TEST_EVAL_FAIL;
      goto END_TEST;
    }

  /* actual NOT equals expected should FAIL */
  if (BZEN_TEST_EVAL_FAIL
      != bzen_test_eval_fn_bool ("testFunction", BZEN_TEST_BOOL_TRUE, BZEN_TEST_BOOL_FALSE, NULL))
    {
      fprintf (
	  stderr,
	  "\n\bzen_test_eval_fn_bool (NULL, BZEN_TEST_BOOL_TRUE, BZEN_TEST_BOOL_FALSE, NULL) \
	   returned BZEN_TEST_EVAL_PASS expected BZEN_TEST_EVAL_FAIL.\n");
      result = BZEN_TEST_EVAL_FAIL;
      goto END_TEST;
    }

  /* actual equals expected should PASS */
  if (BZEN_TEST_EVAL_FAIL == bzen_test_eval_fn_int ("testFunction", 1, 1, NULL))
    {
      fprintf (
	  stderr,
	  "\n\bzen_test_eval_fn_int (NULL, 1, 1, NULL) returned BZEN_TEST_EVAL_FAIL expected BZEN_TEST_EVAL_PASS.\n");
      result = BZEN_TEST_EVAL_FAIL;
      goto END_TEST;
    }

  /* actual NOT equals expected should FAIL */
  if (BZEN_TEST_EVAL_FAIL != bzen_test_eval_fn_int ("testFunction", 1, 0, NULL))
    {
      fprintf (
	  stderr,
	  "\n\bzen_test_eval_fn_int (NULL, 1, 0, NULL) returned BZEN_TEST_EVAL_PASS expected BZEN_TEST_EVAL_FAIL.\n");
      result = BZEN_TEST_EVAL_FAIL;
      goto END_TEST;
    }

  END_TEST:

  return result;
}
