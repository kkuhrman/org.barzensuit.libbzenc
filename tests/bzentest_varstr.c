/**
 * @file:	bzentest_varstr.c
 * @brief:	Unit test stream buffer functions.
 *
 * @copyright:	Copyright (C) 2017 Kuhrman Technology Solutions LLC
 * @license:	GPLv3+: GNU GPL version 3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <stdio.h>

/* libzenc */
#include "bzentest.h"
#include "bzenvarstr.c"

int main (int argc, char *argv[])
{
  int result = BZEN_TEST_EVAL_PASS;
  int test_char = (int)'X';
  int read_char;
  size_t buffer_size = 8;
  char testbuf[256];
  const char* TEST_STR = "This is a test string.";
  
  /**
   * Create a new varstr object.
   */
  bzen_varstr_t* pvarstr = bzen_varstr_create(buffer_size);
  
  #ifdef _BZEN_DEBUG_MODE_
    assert(pvarstr);
  #endif /*_BZEN_DEBUG_MODE_ */
  
  /**
   * Test bzen_varchar_t->bzen_varchar_put()
   */
  bzen_varchar_put(&(pvarstr->varchar), test_char);
  /*fprintf(stderr, "bzen_varchar_put(%s)", test_char);*/
  
  /**
   * Test bzen_varchar_t->bzen_varchar_get()
   */
  read_char = bzen_varchar_get(&(pvarstr->varchar));
  /*fprintf(stderr, "bzen_varchar_put(%s)", read_char);*/
  
  #ifdef _BZEN_DEBUG_MODE_
    /*fprintf(stderr, "pvarstr->varchar.data[0] = %s", pvarstr->varchar.data[0]);*/
    /* assert(read_char == pvarstr->varchar.data[0]); */
  #endif /*_BZEN_DEBUG_MODE_ */
  
  /**
   * bzen_varchar_t->bzen_varchar_unget()
   */
  bzen_varchar_unget(&(pvarstr->varchar), test_char);
  
  /**
   * Test bulk read from buffer.
   */
  bzen_varchar_read(&(pvarstr->varchar), testbuf, pvarstr->varchar.length);
  
  /**
   * Test bulk write to buffer.
   */
  bzen_varchar_write(&(pvarstr->varchar), TEST_STR, BZEN_SIZE(sizeof(TEST_STR)));
  
  /**
   * Test copying one string into another.
   */
  bzen_varstr_strcpy(pvarstr, TEST_STR);
  
  /**
   * Test copying limited number of bytes from one string into another.
   */
  bzen_varstr_strncpy(pvarstr, TEST_STR, BZEN_SIZE(sizeof(TEST_STR))/2);
  
  /* 
   * Test appending one string to another. */
  bzen_varstr_strcat(pvarstr, TEST_STR);
  
  
  /** 
   * Test appending limited number of bytes from one string to another.
   */ 
   bzen_varstr_strncat(pvarstr, TEST_STR, BZEN_SIZE(sizeof(TEST_STR))/2);
  
  /**
   * Delete the varstr object.
   */
  bzen_varstr_free(pvarstr);
    
END_TEST:

  return result;
}