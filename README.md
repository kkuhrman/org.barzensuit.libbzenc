# README #

Copyright (C) 2017 Kuhrman Technology Solutions LLC.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

libbzenc is a shared library written in C which is used for communications between command line tools (e.g. executables) and services (e.g. daemons). 

It is a part of an effort to create a full "free" (as in "freedom") license solution for generating, stooring and regulating electricity from solar energy. To learn more about this effort visit https://barzensuit.org/.

### What is this repository for? ###

This repository comprises all source and related files necessary to build libbzenc using Gnu autotools or create a CDT project in Eclipse with autotools support. It also includes an example program which demonstrates basic usage.

### How do I get set up? ###

#### Basic Gnu Autotools Requirements ####

See the INSTALL file for generic Gnu configure, build and install instructions.

To build libbzenc one will need Gnu autotools at a minimum. The software in this repository was built and tested with the following:
* Gnu libtool 2.4
* Gnu Autoconf 2.69
* Gnu Make 3.82
* GCC 4.83
* GDB 7.6.1
* Gnu libc 2.17

autotools will install libraries in /usr/local/lib and programs in /usr/local/bin by default. This should go without saying (but the frequency of posts relating to "cannot open shared object file: No such file or directory" indicates otherwise): It may be necessary to add your installation dir to your PATH:

e.g. [user@example.com]# PATH=$PATH:/usr/local/bin

It may also be necessary to rebuild your shared object cache with ldconfig:

e.g. [user@example.com]# sudo ldconfig /usr/local/lib

(@see man ldconfig for more on ldconfig)

Best practice is to keep build target directories (where object files etc are written) separate from the source code itself. For example, the developer might clone Git projects in a directory named org.barzensuit/usr/src. Then, the developer would invoke autoreconf, configure and make from a separate directory, say org.barzensuit/usr/local/src/[project name].

To compile with debug symbols: 
[user@example.com]# ./configure CFLAGS=”-g -O0” && make

#### Eclipse CDT/Autotools Requirements ####

The repository also includes files to build libbzenc as an Eclipse CDT/Autotools project. It was tested in Eclipse Oxygen.

In order to avoid the build clutter in your project directories, follow best practice (as above) and create a build directory separate from your project. Set your build directory in your Eclipse project settings by changing the value of Project | Properties | C\C++ Build | Build directory. 

@todo: Dependencies
@todo: Database configuration
@todo: How to run tests
@todo: Deployment instructions

#### Log Files ####

@todo: replace manual procedure with something a bit more elegant.

If user wished to have messages sent to log files s/he must create a directory and set write permissions for user. Set and export environment variable BZEN_LOG_DIR. Otherwise bzentest_log will fail and any messages sent to log facility will be ignored.

### References ###

The following references will likely prove helpful.

To those new to Gnu standards projects (or those who have been away for some time) the "Hello" project is a good place to dive in: https://www.gnu.org/software/hello/. Otherwise an excellent introduction to autotools and the Gnu Build System: https://www.gnu.org/software/automake/manual/html_node/Autotools-Introduction.html.

Gnu Autoconf: https://www.gnu.org/software/autoconf/autoconf.html

Gnu Make: https://www.gnu.org/software/automake/

Gnu Libtool: https://www.gnu.org/software/libtool/libtool.html

Gnu C Reference: https://www.gnu.org/software/libc/manual/html_node/index.html

Gnu coding standards: https://www.gnu.org/prep/standards/html_node/index.html#SEC_Contents

Information for maintainers of Gnu software: https://www.gnu.org/prep/maintain/maintain.html#Top

The Gnu Portability Library: https://www.gnu.org/software/gnulib/

Cnu GetText: https://www.gnu.org/software/gettext/manual/html_node/Files-under-Version-Control.html

GDB: https://www.gnu.org/software/gdb/documentation/

Eclipse CDT/Autotools: https://wiki.eclipse.org/CDT/Autotools/User_Guide

### Contribution guidelines ###

#### Coding Standards and Naming Convention ####

Project Barzensuit shall follow the Gnu coding standards (see references section). By convention all Project Barzensuit files and functions will have the prefix bzen[name]_ where [name] is (ideally) a four character abbreviation of the sub-project name. For example, bzenxmpp_ is the prefix for all files and functions in the Barzensuit XMPP package. File names in libbzenc should comprise the prefix “bzen” (no underscore) with up to four additional characters serving as an abbreviation for the files primary purpose. For example, the file “bzensock.h” contains the declarations for libbzenc's sockets API. The prefix bzen_ is reserved for function names in the Barzensuit C Library. This distinguishes libbzenc functions from from corresponding Gnu or ANSI C library functions for which these might serve as a wrapper. By extension, the developer working in program code, say bzenxmpp, should be able to discern on sight, by way of a prefix, whether a function is local to the project, a member of libbzenc or a Gnu/ANSI library function. 

In a similar manner symbolic constants employ the same prefixes, except in all caps (for example, BZEN_SOME_SYMBOLIC_CONSTANT is defined in libzenc and BZENXMPP_SOME_SYMBOLIC_CONSTANT is defined in the bzenxmpp package.

The main routine (program point of entry) in each package should be defined in the file bzen[name]_main.c and the file name bzen[name]_main.h is reserved for global includes, defines, symbolic constants and so on.

#### Package and Maintainer Standards ####

Project Barzensuit will follow the standards for maintainers of GNU software (see references section). If Gnu Gettext is added to the package (as should be the case in any package containing data in plain English), it is to be added in a folder named “gtext” directly under the project root folder. M4 macros imported by gettextize should be written to gtext/m4. Likewise, any modules imported from the Gnu Portability Library should be written to a folder named “glib”, directly under the project root folder, and corresponding M4 macros to glib/m4. The folder “lib”, directly under the project root folder, should contain other packages included in the distribution (e.g. Gnu TLS), each in its own sub-folder. The folders “inc” and “src”, both directly under the project root folder, should contain package header (.h) and implementation (.c) source files, respectively. The folder “tests”, also directly under the project root folder, is for unit test code.

@todo: Writing tests
@todo: Code review

### Who do I talk to? ###

The barzensuit.org project is sponsored by Kuhrman Technology Solutions LLC. Direct all correspondence to info@kuhrman.com